CREATE PROCEDURE [dbo].[transactionMart_import_example]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	BULK INSERT Staging.dbo.exmample_table
	FROM 'import_file.csv'
	WITH (FORMATFILE='import_table_format.xml')


	WHILE (SELECT COUNT(*) FROM [Staging].[dbo].[example_table]) > 0
		BEGIN

			DROP TABLE IF EXISTS #example_table;

			SELECT * INTO #example_table FROM
			(
				SELECT TOP 200000 *
					FROM [Staging].[dbo].[example_table]
			) t1

				DELETE FROM t1
				FROM TransactionMart.dbo.example_table as t1
				INNER JOIN #example_table as t2
				ON (
					t1.key_one = t2.key_one
					AND t2.key_two = t2.key_two
				);


			INSERT INTO TransactionMart.dbo.example_table
				SELECT * FROM #example_table;

			DELETE FROM t1
			FROM [Staging].[dbo].[example_table] as t1
			INNER JOIN #example_table as t2
			ON (
					t1.key_one = t2.key_one
					AND t2.key_two = t2.key_two
			);
		END

END
-- exec [dbo].[transactionMart_import_example]