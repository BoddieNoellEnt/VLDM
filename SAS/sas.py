import swat
from sqlalchemy import create_engine
from os.path import exists


class SAS:

    sas_user = ''
    sas_pw = ''
    sas_port = 5570
    sas_server = ''
    warehouse_user = ''
    warehouse_pw = ''
    warehouse_server = ''

    def __init__(self):
        self.swat = swat
        self.sas = swat.CAS(self.sas_server, self.sas_port, self.sas_user, self.sas_pw)

        self.metadata = create_engine('mssql+pymssql://%s:%s@%s/Metadata' % (
            self.warehouse_server,
            self.warehouse_user,
            self.warehouse_pw))

        self.warehouse = create_engine('mssql+pymssql://%s:%s@%s/Warehouse'% (
            self.warehouse_server,
            self.warehouse_user,
            self.warehouse_pw))

        self.t_mart = create_engine('mssql+pymssql://%s:%s@%s/TransactionMart'% (
            self.warehouse_server,
            self.warehouse_user,
            self.warehouse_pw))

    def read_from_metadata(self, sql):
        return self.sas.read_sql_query(sql, self.metadata, coerce_float=False)

    def read_from_warehouse(self, sql):
        try:
            return self.sas.read_sql_query(sql, self.warehouse, coerce_float=False)
        except ValueError:
            return False

    def read_from_tmart(self, sql):
        return self.sas.read_sql_query(sql, self.t_mart, coerce_float=False)

    def read_from_sas(self, file_name):
        if exists(file_name):
            return self.sas.read_sas(file_name)
        return False

    def read_from_file(self, file_name):
        if exists(file_name):
            return self.sas.read_csv(file_name)
        return False

    def write_to_tmart(self, data, table):
        return data.to_sql(table, self.t_mart, if_exists="append", index=False)

    @staticmethod
    def write_to_file(table_object, write_header=False):

        if 'index' in table_object.data.columns:
            table_object.data.drop('index', inplace=True, axis=1)

        table_object.data.to_csv(table_object.output_file, index=False, header=write_header)

