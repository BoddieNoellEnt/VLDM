#! /usr/bin/python
from TLogMods import OrderMods
from TLog import Order


def run_order(sas, day):
    order = Order(day, sas=sas, mods=OrderMods)
    sas.write_to_file(order, write_header=False)
    order.migrate_to_database()


if __name__ == '__main__':
    from SAS import SAS
    run_order(SAS(), day='2018-06-15')
