from .TLogMods import TLogMods


class OrderMods(TLogMods):

    drop_columns = ['ColumnD', 'ColumnE']
    rename_columns = [{'ColumnF': 'New_ColumnF'}]
    add_columns = [{'name': 'transaction_type', 'value': None}]
    modify_columns = [
            {'name': 'ColumnA', 'callback': '_convert_to_int'},
            {'name': 'ColumnB', 'callback': '_zero_int'},
            {'name': 'ColumnC', 'callback': '_order_custom_function'},
    ]

    def _order_custom_function(self, column_name):
        self.data[column_name] = self.data[column_name]
