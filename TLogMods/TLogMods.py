import swat


class TLogMods:

    drop_columns = list()
    rename_columns = dict()
    add_columns = dict()
    modify_columns = dict()

    def __init__(self, master_obj, data_location='data'):

        self.master_obj = master_obj
        data = getattr(master_obj, data_location)
        if isinstance(data, swat.CASTable):
            self.data = data.to_frame()
        elif isinstance(data, swat.SASDataFrame):
            self.data = data
        else:
            raise Exception('Incorrect Data Set Provided')

    def transform(self):
        self.transform_drop_columns()
        self.transform_add_columns()
        self.transform_rename_columns()
        self.transform_modify_columns()

    def transform_drop_columns(self):
        try:
            self.data.drop(self.drop_columns, inplace=True, axis=1)
        except ValueError:
            pass

    def transform_rename_columns(self):

        for column_pair in self.rename_columns:
            self.data.rename(column_pair, inplace=True, axis=1)

    def transform_add_columns(self):

        for column in self.add_columns:
            if column['name'] not in self.data.columns:
                self.data[column['name']] = column['value']

    def transform_modify_columns(self):
        for column in self.modify_columns:
            if hasattr(self, column['callback']):
                getattr(self, column['callback'])(column['name'])
        pass

    def _zero_int(self, column_name):
        self.data[column_name] = 0

    def _multiple_100_int(self, column_name):
        self.data[column_name] = self.data[column_name].apply(lambda x: x + 100)

    def _convert_to_int(self, column_name):
        self.data[column_name] = self.data[column_name].astype(int)
