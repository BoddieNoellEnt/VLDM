#! /usr/bin/python
from SAS import SAS
from run_orders import run_order
from datetime import datetime, timedelta


def exec_day(days_back):
    date = str(datetime.strftime(datetime.now() - timedelta(days_back), '%Y-%m-%d')),
    run_order(SAS(), date)


n = 1
while n < 4:
    exec_day(n)
    n = n + 1
