VLDM

This code is a simplified version of what I wrote to use SAS Viya via Python to build 
a process that pulls transactional data out of our warehouse and moves it to out Transactional Data Mart.

The output of the process creates a headless cvs file that is stored on the database server.
It then calls a stored procedure on the database that uses BCP to import the data into a staging area
and simple merge statement to push the data into the data mart.  (Both our Warehouse and Data Mart are on
a sql server and that is the fast way I've found to get data into it. I've placed an example store proc
at stored_proc_example.sql) 


**SAS Module**

A wrapper the the SWAT module and connections to the various data sources needed.

**TLog**

Each table I need to pull from is placed in this module and extends the base TLog class

**TLogMods**

All modifications to the base data set are put here.  