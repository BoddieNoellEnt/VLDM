import subprocess


class TLog:

    sql = None
    mods = None
    data = None
    output_path = None
    command = None

    def __init__(self, date, sas, mods=None):
        self.date = date
        self.sas = sas
        self.mods = mods
        self.get_records()
        self.apply_mods()

    def get_records(self):
        if self.sql is not None:
            self.data = self.sas.read_from_warehouse(self.sql % self.date).to_frame()
        else:
            raise Exception('Cannot Instantiate Parent Class')

    def apply_mods(self):
        if self.data is False:
            return

        if self.mods is None:
            return

        mod = self.mods(self)
        mod.transform()
        self.data = mod.data

    def migrate_to_database(self):
        if self.command is None:
            return
        command = 'sqlcmd -S BNEWH.BODDIENOELL.COM -U Python -P Cb7Ampdpnq -Q "%s"' % self.command
        subprocess.call(command, shell=True)
