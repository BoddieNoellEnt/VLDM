from TLog import  TLog
from os.path import join


class Order(TLog):

    sql = 'SELECT * FROM recent_order_abridged WHERE Business_Date = \'%s\''
    output_file = join('/', 'mnt', 'g', 'warehouse', 'transaction_mart', 'tm_order.csv')
    command = 'EXEC [PollingStaging].[dbo].[transactionMart_import_order]'
